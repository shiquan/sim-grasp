# -*- coding: utf-8 -*-
# Copyright 2015, 2016 Shiquan Wang
# 
# This file is part of SimGrasp.
# 
# SimGrasp is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# SimGrasp is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with SimGrasp.  If not, see <http://www.gnu.org/licenses/>.

"""
    .. codeauthor:: Tyler Packard

    BlenderTransforms is a simple blender script that performs scaling
    transformations on ``.stl`` files.
    Run it with ``blender -b -P BlenderTransforms.py -- FILE -s SCALE [-o
    OUTPUT_FILE]``
"""

import argparse
import bpy
import sys

# Parse arguments
parser = argparse.ArgumentParser(description='Transform stl files for SimGrasp')
parser.add_argument('file', type=str)
parser.add_argument('-o', '--output', default='', type=str)
parser.add_argument('-s', '--scale', default=(1.0, 1.0, 1.0), type=float, nargs=3)
if '--' in sys.argv:
    args = parser.parse_args(sys.argv[sys.argv.index('--') + 1:]) # Ignore args before '--' (blender arguments)

    # Clear all pre-existing objects
    bpy.ops.object.select_all(action='SELECT')
    bpy.ops.object.delete()

    # Load and select object
    bpy.ops.import_mesh.stl(filepath=args.file)
    bpy.ops.object.select_all(action='SELECT')

    # Perform transformation
    bpy.ops.transform.resize(value=args.scale)

    # Save and quit
    if args.output == '':
        args.output = args.file[:args.file.rfind('.')] + '.trns.stl'
    bpy.ops.export_mesh.stl(filepath=args.output, check_existing=False)

bpy.ops.wm.quit_blender()
