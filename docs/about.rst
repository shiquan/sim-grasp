#####
About
#####

SimGrasp is a convenient simulation package built upon `Klamp't`_ for generic dynamic hand simulation. It is capable of batch dynamic simulation for full/under-actuated hand design optimization and can efficiently handle cases of frictional contacts and loop kinematics.

Robotic hand design usually involves optimization of numerous design parameters, including finger configuration, phalanges and palm dimensions, transmission ratios, joint stiffness, preload torque etc.. Some types of hand performance evaluations require dynamic simulation of the grasping process, such as pullout force on irregular object from a grasp, dynamic grasp region etc.. However, most of the dynamic simulation packages out there are not specifically created for this application. Changing the design and relevant parameters require constructing new xml files and changing varibales in multiple files, in which case conducting a large number of simualtions for design optimization is extremely inefficient.

This project aims at providing a convenient tool for generic hand simulation, with which users can create new hand designs and tuning hand-specific parameters with only a few lines of codes and are able to focus more on batch simulation experiments and grasping process design.

This project is developed along with the `Multi Limbed Climbing`_ and `Red Sea Exploratorium`_ BDML research projects at Stanford University.

.. _Klamp't: http://motion.pratt.duke.edu/klampt/index.html
.. _Multi Limbed Climbing: http://bdml.stanford.edu/Main/MultiLimbedClimbing
.. _Red Sea Exploratorium: http://bdml.stanford.edu/Main/KaustRedSea
