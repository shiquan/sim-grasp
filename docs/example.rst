#############
Example Guide
#############

Introduction
============

Example code corresponding to this guide can be found in the file ``example.py``
in the git repository. For reference, the hierarchy of SimGrasp has the
following structure:

.. image:: /_static/diagram.png
   :scale: 50%
   :align: center

To start, import the SimGrasp module into your Python 2 program, called
``simgrasp``. If it cannot be found, make sure you have run ``sudo python
setup.py install`` first to install SimGrasp. In order to run a simulation, you
must override :class:`GraspSimulation.GraspStateMachine` and
:class:`GraspSimulation.BatchSim` with your own simulation code.

Creating a State Machine
========================

The state machine controls which state the simulation is in and is responsible
for determining when the simulation is complete. To create your own state
machine, override the class :class:`GraspSimulation.GraspStateMachine`. The
state is stored as a string in the ``curState`` member variable and can be set
by using the :func:`GraspSimulation.GraspStateMachine.enterState` method. All
states must first be added to a list stored in the ``states`` member variable or
else the state will not be able to be entered. The states are managed in the
:func:`GraspSimulation.GraspStateMachine.runStateMachine` method along with
logic detailing what to do under each state. For example, in example.py, the
method contains a series of if/elif statements to check for which state the
state machine is in. Within each if statement, different torques and forces are
applied to the :class:`Design.Finger`\ s and :class:`Design.GraspObject` and the
state is changed if certain conditions are met. When the simulation is complete,
the variable ``simIsDone`` is set to ``True`` so that the next simulation can
begin. No further cleanup is necessary, as it is all managed by the
:class:`GraspSimulation.GraspSimulation` class. The method also uses a few
helper methods that determine wether or not conditions for changing states have
been met. Using helper methods like these are optional, but may be helpful for
code reuse.

Creating a Batch Simulation
===========================

Batch simulations allow you to run multiple simulations with slightly different
hand parameters in each simulation. To create a batch simulation, override the
class :class:`GraspSimulation.BatchSimulation`. The batch simulation must define
a ``finger``, ``hand``, ``graspObject``, ``handController``, and
``graspStateMachine`` for the :class:`GraspSimulation.GraspSimulation` to use.
The method :func:`GraspSimulation.BatchSim.importNewSim` is used to generate new
simulations. In the example code, the method generates new hand and finger files
along with a new :mod:`HandController`. It keeps track of which simulation
number is running with the ``iterNum`` variable and changes the orientation of
the fingers in each simulation. The batch simulation must also define a
:func:`GraspSimulation.BatchSim.deleteOldSim` and
:func:`GraspSimulation.BatchSim.endBatchSim` method.
:func:`GraspSimulation.BatchSim.deleteOldSim` method should delete the
``finger``, ``hand``, ``graspObject``, ``handController``, and
``graspStateMachine`` objects. The method
:func:`GraspSimulation.BatchSim.endBatchSim` is called once all of the
simulations have finished. In the example code, this method is used to write
the results of all of the simulations to a file. To run a batch simulation,
first create a new instance of your :class:`GraspSimulation.BatchSim`. Then
create a new instance of a :class:`GraspSimulation.GraspSimulation` and pass
the batch simulation object as an argument and call
:func:`GraspSimulation.GraspSimulation.run`.
