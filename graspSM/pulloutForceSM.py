# -*- coding: utf-8 -*-
"""
    Copyright 2015, 2016 Shiquan Wang
    
    This file is part of SimGrasp.

    SimGrasp is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SimGrasp is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SimGrasp.  If not, see <http://www.gnu.org/licenses/>.
"""

"""
Created on Thu Dec 24 17:17:09 2015

@author: shiquan
"""
import sys
import numpy

class GraspStateMachine:
    def __init__(self, graspTorque = 1):
        self.states = ['Free_Grasping', 'Object_Grasping', 'Object_Pulling', 'Finished']
        self.graspTorque = graspTorque
        self.pullForce = 0
        self.enterState('Free_Grasping')
        self.textToDisplay = ''
        self.simIsDone = False
        self.result = None
    """
    Run the state machine by one step
    - ### Write your grasp state machine here ###
    """
    def runStateMachine(self, currentTime, handController, graspObject):  
        """Updating all varaibles"""
        [objAnguVel, objTranVel] = graspObject.getVelocity()
        [objRotMat, objPos] = graspObject.getTransform()
        
        """State machine"""
        #closing the hand, before contacting the object
        if self.curState == 'Free_Grasping':
            handController.setDofTorque((0, 0), self.graspTorque)
            handController.setDofTorque((1, 0), self.graspTorque)            
            handController.setDofTorque((2, 0), self.graspTorque)
            #when touching the object (the object start to move)
            if self.objIsMoving(objAnguVel, objTranVel):
                self.enterState('Object_Grasping')

        #hand is grasping the object
        elif self.curState == 'Object_Grasping':
            handController.setDofTorque((0, 0), self.graspTorque)
            handController.setDofTorque((1, 0), self.graspTorque)            
            handController.setDofTorque((2, 0), self.graspTorque)
            #when the grasp enters equilibrium state, start pulling the object
            if self.objIsStill(objAnguVel, objTranVel):
                self.enterState('Object_Pulling')
            #when the object escaping away from the hand, finish the simulation
            elif self.objHasEscaped(objPos):
                self.enterState('Finished')
                
        #pulling the object along z axis, increase the pulling force when the object is still
        elif self.curState == 'Object_Pulling':
            handController.setDofTorque((0, 0), self.graspTorque)
            handController.setDofTorque((1, 0), self.graspTorque)            
            handController.setDofTorque((2, 0), self.graspTorque)
            if self.objHasEscaped(objPos):
                self.enterState('Finished')
            #when the object becomes still, increase the pulling force
            elif self.objIsStill(objAnguVel, objTranVel):
                self.pullForce += 1
                print 'Pulling force is now:', self.pullForce
            #apply pulling force to the object, along z direction, at point in local coordinates
            graspObject.applyForceAtLocalPoint([0, 0, self.pullForce], [0, 0, 0]) 
                
        #when the simulation is finished
        elif self.curState == 'Finished':
            print 'Simulation Finished!'
            print 'Grasp Torque:', self.graspTorque, 'Maximum Pulling Force:', self.pullForce
            self.simIsDone = True
        
        self.textToDisplay = 'Current State: ' + self.curState + '\n' + 'Pullout Force: ' + str(self.pullForce)
        #print handController.getTendonPos((0, 0))
        #save the parameters that you want to log here
        self.result = [currentTime, self.pullForce]
        
    def enterState(self, newState):
        assert (newState in self.states), "The new state is not in the states list!"
        self.curState = newState
        print 'Current State:', newState
        
    """Helper function to query if the object has escaped away from the hand"""
    def objHasEscaped(self, objPos):
        if objPos[2] > 0.5: #z direction
            return True
        else:
            return False
    """Helper function to query if the object becomes still""" 
    def objIsStill(self, objAnguVel, objTranVel):
        #print numpy.linalg.norm(objAnguVel), numpy.linalg.norm(objTranVel)
        if numpy.linalg.norm(objAnguVel) < 0.005 and numpy.linalg.norm(objTranVel) < 0.0005:
            return True
        else:
            return False
    """Helper function to query if the object starts to move""" 
    def objIsMoving(self, objAnguVel, objTranVel):
        #print numpy.linalg.norm(objAnguVel), numpy.linalg.norm(objTranVel)
        if numpy.linalg.norm(objAnguVel) > 0.01 or numpy.linalg.norm(objTranVel) > 0.001:
            return True
        else:
            return False